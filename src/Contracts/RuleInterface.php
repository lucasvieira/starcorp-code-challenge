<?php
declare(strict_types=1);

namespace Starcorp\Contracts;

/**
 * RuleInterface
 *
 * @package Starcorp\Contracts
 */
interface RuleInterface
{
    /**
     * Verifies if a number matches a given condition
     *
     * @param int $number
     * @return bool
     */
    function matches(int $number): bool;

    /**
     * Return class string
     *
     * @return string
     */
    function getString(): string;
}