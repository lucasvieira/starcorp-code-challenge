<?php
declare(strict_types=1);

namespace Starcorp;

use Starcorp\Rules\IT;
use Starcorp\Rules\StarCorpianos;
use Starcorp\Rules\Starcorp as StarCorpRule;

/**
 * Create a FizzBuzz-like object with tests specifications
 *
 * @package Starcorp
 */
class StarcorpFactory
{
    public static function create(): Starcorp
    {
        $instance = new Starcorp();

        $instance->addRule(new StarCorpianos());
        $instance->addRule(new IT());
        $instance->addRule(new StarCorpRule());

        return $instance;
    }
}