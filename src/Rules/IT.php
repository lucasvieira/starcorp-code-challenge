<?php
declare(strict_types=1);

namespace Starcorp\Rules;

use Starcorp\Contracts\RuleInterface;

/**
 * IT Rule
 * Matches multiples of 5
 *
 * @package Starcorp\Rules
 */
class IT implements RuleInterface
{
    /**
     * Verifies if a number is a multiple of 5
     *
     * @param int $number
     * @return bool
     */
    function matches(int $number): bool
    {
        return $number % 5 === 0;
    }

    /**
     * Return class string
     *
     * @return string
     */
    function getString(): string
    {
        return "IT";
    }
}