<?php
declare(strict_types=1);

namespace Starcorp\Rules;

use Starcorp\Contracts\RuleInterface;

/**
 * StarCorp Rule
 * Matches multiples of 3
 *
 * @package Starcorp\Rules
 */
class StarCorp implements RuleInterface
{
    /**
     * Verifies if a number is multiple of 3
     *
     * @param int $number
     * @return bool
     */
    function matches(int $number): bool
    {
        return $number % 3 === 0;
    }

    /**
     * Return class string
     *
     * @return string
     */
    function getString(): string
    {
        return "StarCorp";
    }
}