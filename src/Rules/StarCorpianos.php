<?php
declare(strict_types=1);

namespace Starcorp\Rules;

use Starcorp\Contracts\RuleInterface;

/**
 * StarCorpianos Rule
 * Matches multiples of 3 and 5
 *
 * @package Starcorp\Rules
 */
class StarCorpianos implements RuleInterface
{
    /**
     * Verifies if a number is multiple of 3 and 5
     *
     * @param int $number
     * @return bool
     */
    function matches(int $number): bool
    {
        return $number % 3 === 0 && $number % 5 === 0;
    }

    /**
     * Return class string
     *
     * @return string
     */
    function getString(): string
    {
        return "StarCorpianos";
    }

}