<?php
declare(strict_types=1);

namespace Starcorp;

use Starcorp\Contracts\RuleInterface;
use Starcorp\Exceptions\NegativeOrZeroIntegerException;

/**
 * Starcorp
 *
 * @package Starcorp
 */
class Starcorp
{
    /**
     * Rules to test numbers against
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Adds a new rule for the class
     *
     * @param RuleInterface $rule
     */
    public function addRule(RuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * Return all rules
     *
     * @return RuleInterface[]
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * Generate sequence
     *
     * @param int $number
     * @return string
     * @throws NegativeOrZeroIntegerException
     */
    public function generate(int $number): string
    {
        if ($number <= 0) {
            throw new NegativeOrZeroIntegerException($number);
        }

        $output = [];
        for ($i = 1; $i <= $number; $i++) {
            $output[] = $this->getString($i);
        }

        return implode(PHP_EOL, $output);
    }

    /**
     * Check a given number against the given rules and
     * return the correspondent string.
     * If the number doesn't matches any rule, return the number as a string.
     *
     * @param int $value
     * @return string
     */
    public function getString(int $value): string
    {
        foreach ($this->getRules() as $rule) {
            if ($rule->matches($value)) {
                return $rule->getString();
            }
        }

        return (string)$value;
    }
}