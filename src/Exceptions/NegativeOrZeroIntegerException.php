<?php
declare(strict_types=1);

namespace Starcorp\Exceptions;

/**
 * Custom exceptions for negative numbers or zero given for some method
 *
 * @package Starcorp\Exceptions
 */
class NegativeOrZeroIntegerException extends \Exception
{
    public function __construct(int $value)
    {
        parent::__construct("Invalid integer given: $value. You must give a positive integer.");
    }
}