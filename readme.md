# Starcorp code challenge

## Requirements

* PHP 7.3.0 or above.

## Installing

Very simple. After cloning this repository, you should install PHP dependencies with composer.  
`composer install`

## Running tests

To run unit tests of project:  
`vendor/bin/phpunit -c phpunit.xml` 

## Usage
After installing the dependencies, you can use the class `StarcorpFactory` to get an instance of the `Starcorp` class that generates the FizzBuzz-like sequence as required by the challenge.

```(php)
<?php

require_once 'vendor/autoload.php';

$starCorp = \Starcorp\StarcorpFactory::create();

echo $starCorp->generate(100) . PHP_EOL;
`````

To modify the rules of the test, you can create an instance of the `Starcorp` class directly.

```(php)
<?php

require_once 'vendor/autoload.php';

$starCorp = new \Starcorp\Starcorp();

// Add custom rules
$starCorp->addRule(new \Starcorp\Rules\IT());
$starCorp->addRule(new \Starcorp\Rules\StarCorp());

echo $starCorp->generate(100) . PHP_EOL;
`````


