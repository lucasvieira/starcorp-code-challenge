<?php
declare(strict_types=1);

namespace Unit;

use Starcorp\Starcorp;
use Starcorp\Rules\IT;
use Starcorp\Rules\StarCorpianos;
use Starcorp\Rules\StarCorp as StarCorpRule;
use Starcorp\Exceptions\NegativeOrZeroIntegerException;

class StarcorpTest extends TestCase
{
    public function testGenerateThrowsExceptionWithNegativeInteger()
    {
        $this->expectException(NegativeOrZeroIntegerException::class);
        $integer = random_int(1, PHP_INT_MAX) * (-1); // negative

        $starcorp = new Starcorp();
        $starcorp->generate($integer);
    }

    public function testGenerateThrowsExceptionWithZero()
    {
        $this->expectException(NegativeOrZeroIntegerException::class);
        $starcorp = new Starcorp();
        $starcorp->generate(0);
    }

    public function testAddRule()
    {
        $starcorp = new Starcorp();
        $this->assertCount(0, $starcorp->getRules());

        $starcorp->addRule(new IT());
        $this->assertCount(1, $starcorp->getRules());
    }

    public function testGetRules()
    {
        $starcorp = new Starcorp();
        $starcorp->addRule(new IT());
        $rules = $starcorp->getRules();
        $this->assertCount(1, $rules);
        $this->assertInstanceOf(IT::class, array_pop($rules));
    }

    public function testGetStringWithNoRules()
    {
        $starcorp = new Starcorp();

        $this->assertEquals("10", $starcorp->getString(10));
        $this->assertEquals("12", $starcorp->getString(12));
        $this->assertEquals("15", $starcorp->getString(15));
        $this->assertEquals("32", $starcorp->getString(32));
    }

    public function testGetStringWithRules()
    {
        $starcorp = new Starcorp();

        $starcorp->addRule(new StarCorpianos());
        $starcorp->addRule(new IT());
        $starcorp->addRule(new StarCorpRule());

        $this->assertEquals("IT", $starcorp->getString(10));
        $this->assertEquals("IT", $starcorp->getString(20));
        $this->assertEquals("StarCorp", $starcorp->getString(12));
        $this->assertEquals("StarCorp", $starcorp->getString(18));
        $this->assertEquals("StarCorpianos", $starcorp->getString(15));
        $this->assertEquals("StarCorpianos", $starcorp->getString(45));
        $this->assertEquals("32", $starcorp->getString(32));
        $this->assertEquals("37", $starcorp->getString(37));
    }

    public function testGenerate()
    {
        $expected = [
            "1",
            "2",
            "StarCorp",
            "4",
            "IT",
            "StarCorp",
            "7",
            "8",
            "StarCorp",
            "IT",
            "11",
            "StarCorp",
            "13",
            "14",
            "StarCorpianos"
        ];
        $expected = implode(PHP_EOL, $expected);

        $starcorp = new Starcorp();

        $starcorp->addRule(new StarCorpianos());
        $starcorp->addRule(new IT());
        $starcorp->addRule(new StarCorpRule());

        $this->assertEquals($expected, $starcorp->generate(15));
    }
}