<?php
declare(strict_types=1);

namespace Unit\Rules;

use Unit\TestCase;
use Starcorp\Rules\StarCorpianos;

class StarCorpianosTest extends TestCase
{
    public function testMatches()
    {
        $itRule = new StarCorpianos();

        $this->assertTrue($itRule->matches(15));
        $this->assertTrue($itRule->matches(45));
        $this->assertTrue($itRule->matches(60));

        $this->assertFalse($itRule->matches(18));
        $this->assertFalse($itRule->matches(12));
        $this->assertFalse($itRule->matches(10));
    }

    public function testGetString()
    {
        $itRule = new StarCorpianos();
        $this->assertEquals("StarCorpianos", $itRule->getString());
    }
}