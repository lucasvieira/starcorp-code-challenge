<?php
declare(strict_types=1);

namespace Unit\Rules;

use Unit\TestCase;
use Starcorp\Rules\StarCorp;

class StarCorpTest extends TestCase
{
    public function testMatches()
    {
        $itRule = new StarCorp();

        $this->assertTrue($itRule->matches(9));
        $this->assertTrue($itRule->matches(24));
        $this->assertTrue($itRule->matches(33));

        $this->assertFalse($itRule->matches(17));
        $this->assertFalse($itRule->matches(32));
        $this->assertFalse($itRule->matches(40));
    }

    public function testGetString()
    {
        $itRule = new StarCorp();
        $this->assertEquals("StarCorp", $itRule->getString());
    }
}