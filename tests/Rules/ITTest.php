<?php
declare(strict_types=1);

namespace Unit\Rules;

use Unit\TestCase;
use Starcorp\Rules\IT;

class ITTest extends TestCase
{
    public function testMatches()
    {
        $itRule = new IT();

        $this->assertTrue($itRule->matches(10));
        $this->assertTrue($itRule->matches(25));
        $this->assertTrue($itRule->matches(35));

        $this->assertFalse($itRule->matches(16));
        $this->assertFalse($itRule->matches(21));
        $this->assertFalse($itRule->matches(27));
    }

    public function testGetString()
    {
        $itRule = new IT();
        $this->assertEquals("IT", $itRule->getString());
    }
}