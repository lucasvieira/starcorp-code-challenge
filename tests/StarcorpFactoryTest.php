<?php
declare(strict_types=1);

namespace Unit;

use Starcorp\Starcorp;
use Starcorp\StarcorpFactory;

class StarcorpFactoryTest extends TestCase
{
    public function testCreate()
    {
        $instance = StarcorpFactory::create();
        $this->assertInstanceOf(Starcorp::class, $instance);
        $this->assertCount(3, $instance->getRules());
    }
}